from django.db import models
from django.urls import reverse
from django.utils.text import slugify


# Create your models here.


class ProductCategory(models.Model):
    title = models.CharField(max_length=300, verbose_name='عنوان', db_index=True)
    url_title = models.CharField(max_length=300, verbose_name='عنوان در url', db_index=True)
    is_active = models.BooleanField(verbose_name='فعال / غیرفعال')
    is_delete = models.BooleanField(verbose_name="حذف شده / نشده")

    def __str__(self):
        return f'({self.title} - {self.url_title})'

    class Meta:
        verbose_name = 'دسته بندی'
        verbose_name_plural = 'دسته بندی ها'


class ProductBrand(models.Model):
    title = models.CharField(max_length=300, verbose_name='نام برنپ', db_index=True)
    is_active = models.BooleanField(verbose_name='فعال / غیرفعال')

    class Meta:
        verbose_name_plural = 'برندها'
        verbose_name = 'برند'

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=300, verbose_name='عنوان')
    category = models.ManyToManyField(
        ProductCategory,
        verbose_name='دسته بندی ها',
        related_name='product_categories')
    image = models.ImageField(upload_to='images/products', null=True, blank=True, verbose_name='تصویر محصولات')
    brand = models.ForeignKey(ProductBrand, verbose_name='نام برنپ', null=True, blank=True, on_delete=models.CASCADE)
    price = models.IntegerField(verbose_name='قیمت')
    short_description = models.CharField(max_length=360, null=True, verbose_name='توضیح کوتاه', db_index=True)
    description = models.TextField(verbose_name='توضیح کامل', db_index=True)
    slug = models.SlugField(default="", null=False, db_index=True, max_length=200, unique=True)
    is_active = models.BooleanField(verbose_name='فعال / غیرفعال')
    is_delete = models.BooleanField(verbose_name="حذف شده / نشده")

    def get_absolute_url(self):
        return reverse('product_detail', args=[self.slug])

    # def save(self, *args, **kwargs):
    #     # self.slug = slugify(self.title)
    #     super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.title} - ({self.price})"

    class Meta:
        verbose_name = 'محصول'
        verbose_name_plural = 'محصولات'


class ProductTag(models.Model):
    caption = models.CharField(max_length=200, verbose_name='عنوان', db_index=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_tags')

    def __str__(self):
        return self.caption

    class Meta:
        verbose_name_plural = 'تگ های محصولات'
        verbose_name = 'تگ های محصول'
