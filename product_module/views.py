from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
from django.views import View

from .models import Product, ProductCategory
# from django.http import Http404
# from django.db.models import Avg


# class ProductListView(TemplateView):
#     template_name = 'product_module/product_list.html'
#
#     def get_context_data(self, **kwargs):
#         product = Product.objects.all().order_by('price')[:5]
#         context = super(ProductListView, self).get_context_data()
#         context['product'] = product
#         return context


# class ProductDetailView(TemplateView):
#     template_name = 'product_module/product_detail.html'
#
#     def get_context_data(self, **kwargs):
#         slug = kwargs['slug']
#         product = get_object_or_404(Product, slug=slug)
#         context = super(ProductDetailView, self).get_context_data()
#         context['product'] = product
#         return context


class ProductListView(ListView):
    template_name = 'product_module/product_list.html'
    model = Product
    context_object_name = 'product'
    ordering = ['price']
    paginate_by = 1

    def get_queryset(self):
        base_query = super(ProductListView, self).get_queryset()
        data = base_query.filter(is_active=True)
        return data


class ProductDetailView(DetailView):
    template_name = 'product_module/product_detail.html'
    model = Product
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        loaded_product = self.object
        request = self.request
        favorite_product_id = request.session.get("product_favorite")
        context['is_favorite'] = favorite_product_id == str(loaded_product.id)
        return context


class AddProductFavorite(View):
    def post(self, request):
        product_id = request.POST['product_id']
        product = Product.objects.get(pk=product_id)
        request.session['product_favorite'] = product_id
        return redirect(Product.get_absolute_url(product))


# def product_list(request):
#     product = Product.objects.all().order_by('price')[:5]
#     return render(request, 'product_module/product_list.html', context={'product': product})


# def product_detail(request, slug):
#     # try:
#     #     product = Product.objects.get(pk=product_id)
#     # except:
#     #     raise Http404()
#     product = get_object_or_404(Product, slug=slug)
#     return render(request, 'product_module/product_detail.html', context={'product': product})
