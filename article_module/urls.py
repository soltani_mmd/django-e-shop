from django.urls import path
from . import views

urlpatterns = [
    path('', views.ArticlesListView.as_view(), name='articles_page'),
    path('cat/<str:category>', views.ArticlesListView.as_view(), name='articles_by_categories_list')

]
