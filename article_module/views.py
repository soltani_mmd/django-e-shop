from django.http import HttpRequest
from django.shortcuts import render
from django.views.generic.list import ListView

from article_module.models import Article, ArticleCategory


# Create your views here.


class ArticlesListView(ListView):
    template_name = 'article_module/articles_list.html'
    model = Article
    paginate_by = 5

    def get_context_data(self,  *args, **kwargs):
        context = super(ArticlesListView, self).get_context_data(*args, **kwargs)
        return context

    def get_queryset(self):
        query = super(ArticlesListView, self).get_queryset()
        category_name = self.kwargs.get('category')
        if category_name is not None:
            query = query.filter(selected_categories__url_title__iexact=category_name)
            return query


def article_categories_component(request: HttpRequest):
    article_main_categories = ArticleCategory.objects.filter(is_active=True, parent_id=None)
    context = {
        'main_categories': article_main_categories,

    }
    return render(request, 'article_module/component/article_categories_component.html', context)
