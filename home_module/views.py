from django.shortcuts import render
from django.views import View
from django.views.generic.base import TemplateView

from site_module.models import SiteSetting, FooterLinkBox, SlidersModel


# Create your views here.


# def index_page(request):
#     return render(request, 'home_module/index_page.html')


# class HomeView(View):
#     def get(self, request):
#         return render(request, 'home_module/index_page.html')
class HomeView(TemplateView):
    template_name = 'home_module/index_page.html'  # template view is important for practice#

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sliders: SlidersModel = SlidersModel.objects.filter(is_active=True)
        context['sliders'] = sliders
        return context

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['data'] = "hi whats up bro?!"
    #     return context


def contact_page(request):
    return render(request, 'home_module/contact_page.html')


def site_header_component(request):
    site_settings: SiteSetting = SiteSetting.objects.filter(is_main_setting=True).first()
    context = {
        'site_settings': site_settings
    }
    return render(request, 'shared/site_header_component.html', context)


def site_footer_component(request):
    site_settings: SiteSetting = SiteSetting.objects.filter(is_main_setting=True).first()
    footer_link_boxes: FooterLinkBox = FooterLinkBox.objects.all()

    context = {
        'site_settings': site_settings,
        'footer_link_boxes': footer_link_boxes

    }
    return render(request, 'shared/site_footer_component.html', context)


class AboutView(TemplateView):
    template_name = 'home_module/about_page.html'

    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data(**kwargs)
        settings: SiteSetting = SiteSetting.objects.filter(is_main_setting=True).first()
        context['settings'] = settings
        return context
