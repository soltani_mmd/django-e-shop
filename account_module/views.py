from django.http import Http404, HttpRequest
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from .forms import RegisterForm, LoginForm, ForgotPasswordForm, ResetPasswordForm
from .models import User
from django.utils.crypto import get_random_string
from django.contrib.auth import login, logout
from utils.email_service import send_email


# Create your views here.

class RegisterView(View):
    def get(self, request):
        register_form = RegisterForm()
        context = {
            'register_form': register_form
        }
        return render(request, 'account_module/register.html', context=context)

    def post(self, request):
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            user_email = register_form.cleaned_data.get('email')
            user_password = register_form.cleaned_data.get('password')
            #  for register uniq user is important
            user: bool = User.objects.filter(email__iexact=user_email).exists()
            if user:
                register_form.add_error('email', "ایمیل وارد شده تکراری می باشد")
            else:
                new_user = User(
                    email=user_email,
                    email_active_code=get_random_string(72),
                    is_active=False,
                    username=user_email)
                new_user.set_password(user_password)
                new_user.save()
                # todo: send email active code
                send_email('فعالسازی حساب کاربری', new_user.email, {'user': new_user}, 'emails/active_account.html')
                return redirect(reverse('login_page'))

            context = {
                'register_form': register_form
            }

            return render(request, 'account_module/register.html', context)


class LoginView(View):
    def get(self, request):
        login_form = LoginForm()
        context = {
            'login_form': login_form
        }
        return render(request, 'account_module/login.html', context=context)

    def post(self, request):
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            user_email = login_form.cleaned_data.get('email')
            user_pass = login_form.cleaned_data.get('password')
            user: User = User.objects.filter(email__iexact=user_email).first()
            if user is not None:
                if not user.is_active:
                    login_form.add_error("email", "رمز عبور یا ایمیل اشتباه است")
                else:
                    is_pass_correct = user.check_password(user_pass)
                    if is_pass_correct:
                        login(request, user)
                        return redirect(reverse("home_page"))
                    else:
                        login_form.add_error("email", "رمز عبور یا ایمیل اشتباه است")

            else:
                login_form.add_error("email", "رمز عبور یا ایمیل اشتباه است")

        context = {
            'login_form': login_form
        }
        return render(request, 'account_module/login.html', context=context)

    # # csrf_protected_method = method_decorator(csrf_protect)
    # csrf_secret = request.COOKIES[settings.CSRF_COOKIES_NAME]
    # if login_form.is_valid():
    #     user_email = login_form.cleaned_data.get('email')
    #     user_pass = login_form.cleaned_data.get('password')
    #     user = authenticate(request, username=user_email, password=user_pass)
    #     if user is not None:
    #         login(request, user)
    #         return redirect(reverse("home_page"))
    #     return redirect(reverse('login_page'))


class ActivateAccountView(View):
    def get(self, request, email_active_code):
        user: User = User.objects.filter(email_active_code__iexact=email_active_code).first()
        if user is not None:
            if not user.is_active:
                user.is_active = True
                user.email_active_code = get_random_string(72)
            user.save()
            return redirect(reverse('login_page'))
        else:
            pass

        raise Http404

    def post(self, request: HttpRequest):
        pass


class ForgotPasswordView(View):
    def get(self, request):
        forget_pass_form = ForgotPasswordForm()
        context = {
            'forget_pass_form': forget_pass_form
        }
        return render(request, 'account_module/forgot_pass.html', context)

    def post(self, request: HttpRequest):
        forget_pass_form = ForgotPasswordForm(request.POST)
        if forget_pass_form.is_valid():
            user_email = forget_pass_form.cleaned_data.get('email')
            user: User = User.objects.filter(email__iexact=user_email).first()
            if user is not None:
                # todo: send reset pass email
                send_email('بازیابی حساب کاربری', user.email, {'user': user}, 'emails/forgot_account.html')
                return redirect(reverse('home_page'))

        context = {
            'forget_pass_form': forget_pass_form
        }
        return render(request, 'account_module/forgot_pass.html', context)


class ResetPasswordView(View):
    def get(self, request, active_code):
        user: User = User.objects.filter(email_active_code__iexact=active_code).first()
        if user is None:
            return redirect(reverse('login_page'))

        reset_pass_form = ResetPasswordForm()
        context = {'reset_pass_form': reset_pass_form, 'user': user}
        return render(request, "account_module/reset_pass.html", context)

    def post(self, request: HttpRequest, active_code):
        reset_pass_form = ResetPasswordForm(request.POST)
        user: User = User.objects.filter(email_active_code__iexact=active_code).first()
        if reset_pass_form.is_valid():
            if user is None:
                return redirect(reverse('login_page'))
            user_pass = reset_pass_form.cleaned_data.get('password')
            user.email_active_code = get_random_string(72)
            user.is_active = True
            user.set_password(user_pass)
            user.save()
            return redirect(reverse('login_page'))

        context = {
            'reset_pass_form': reset_pass_form,
            'user': user
        }
        return render(request, "account_module/reset_pass.html", context)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect(reverse('login_page'))
